const fs = require('fs');
const memcache = require('memory-cache');

const root = 'cache/';
let caches = {avatars: [new memcache.Cache(), 'avatars.json'], 
              basicUserInfo: [new memcache.Cache(), 'basicUserInfo.json'],
              setToMapIDs: [new memcache.Cache(), 'setToMapIDs.json'],
              graphURLs: [new memcache.Cache(), 'graphURLs.json']};

for([name,[cache,path]] of Object.entries(caches)) {
    try {
        const cont = fs.readFileSync(root+path);
        caches[name][0].importJson(cont.toString());
    } catch(err) {
        console.log(err);
    }
}

module.exports.get = (cache, key) => {
    key = key.toString();
    return caches[cache][0].get(key);
};

module.exports.set = (cache, key, value, time) => {
    key = key.toString();
    try {
        caches[cache][0].put(key, value, time);
        console.debug(`${cache}[${key}] := ${value}`);
    } catch(err) {
        console.debug(err);
    }
    fs.writeFile(root+caches[cache][1], caches[cache][0].exportJson(), (err) => {
        if(err)
            console.debug(err);
        else
            console.debug(`Cache updated @${caches[cache][1]}`);
    })
}