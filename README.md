# skyBot

A Discord bot for [osu!](https://osu.ppy.sh), focused around showing scores to your friends. It's unfortunately in Russian, and is not available as a public bot, so you would have to host it yourself.

# Running the bot

You'll have to set three environment variables (or five for an extra feature):

- `DISCORD_TOKEN` - your bot token for Discord
- `OSU_TOKEN` - your API key for osu!
- `ADMIN_ID` - the Discord ID of the user whom the bot considers to be admin (this is just used to report errors to you)

Optional if you need graphs (see features below):

- `CLOUDINARY_KEY` - your API key for Cloudinary
- `CLOUDINARY_SECRET` - the corresponding API secret

# Binding your Discord ID to osu! username

Many commands require the osu! username of the player whose information you are trying to look up. However, more often than not you just want that player to be yourself.

To avoid having to type your own name every time, you can associate your Discord ID with your username by adding it in `userContext.json`.

Then, for example, you will be able to write just `!u` instead of `!u your-name` to see your stats.

# Features

All of the commands start with an exclamation mark (`!`).

## User info

`!user username` / `!u username` displays information about a user with the given name:

![](screenshots/user_command.png)

If you have bound your Discord ID to your username, simply writing `!u` will show your own stats.

## Beatmap info

`!link-to-beatmap`, e.g. `!https://osu.ppy.sh/beatmapsets/1/#osu/75`, displays information about a beatmap:

![](screenshots/beatmap_command.png)

If you give the command a link to just a beatmap**set**, e.g. `!https://osu.ppy.sh/beatmapsets/1/`, it will show the beatmap with the highest difficulty in the set.

This command also supports shorthand notation where you just specify the beatmap ID: `!#b75#`, or the beatmapset ID: `!#s1#`.

Finally, adding a `/g` switch to the command also generates a difficulty graph for the map. This function requires you to have set the `CLOUDINARY_KEY` and `CLOUDINARY_SECRET` environment variables, as it depends on Cloudinary for hosting the graph image.

![](screenshots/beatmap_with_graph.png)

## User's latest score

`!l username` (alternative names: `!last`, `!latest`) displays the latest score a given player has set.

![](screenshots/latest_command.png)

If you have bound your Discord ID to your username, simply writing `!l` will show your own latest score.

## User's latest _failed_ score

`!f username` (alternative names: `!fail`, `!failed`) displays the latest failed score a player has.

![](screenshots/failed_command.png)

If you have bound your Discord ID to your username, simply writing `!f` will show your own latest failed score.

## User's best score on a beatmap

`!b beatmap username +mods` (also: `!best`) displays the given user's best score on the given beatmap with the given mods (see [Specifying mods](#specifying-mods)).

The order of the arguments does not matter.

Omitting mods will just show the player's best score on the map.

If you have bound your Discord ID to your username, omitting the username will have the bot look for your own score on the map.

Omitting the beatmap will have the bot use the latest beatmap that appeared in the same Discord channel.

You can specify the beatmap using a link or via the shorthand notation using only the beatmap's ID (e.g. `#b75#` for beatmap with ID 75).

![](screenshots/best_command.png)

## Beatmap leaderboard

`!t beatmap +mods` (also: `!top`) displays the top 3 scores on the given beatmap.

The number of scores can be specified by adding a number right after the command: e.g. `!t10 ...` will show the top 10 scores. This number is limited to 20.

See [Specifying mods](#specifying-mods) on info about how to specify mods. Omitting mods will just show the normal leaderboard.

As above, omitting the beatmap will have the bot use the latest beatmap that appeared in the same Discord channel.

You can specify the beatmap using a link or via the shorthand notation using only the beatmap's ID (e.g. `#b75#` for beatmap with ID 75).

![](screenshots/top_command.png)
![](screenshots/top10_command.png)

## Searching for maps

(This feature doesn't work anymore, as the website it depended on has gone down.)

All commands that required you to specify a beatmap used to allow you to search for the beatmap by its name instead of just expecting a link to it.

The syntax was `find(artist - name [difficulty], author)`, where every field other than the name was optional.

For example, you could write `!t5 find(freedom dive [four dimensions])` to see the top 5 scores on the iconic freedom dive beatmap.

## Reading maps from a screenshot

(This feature also doesn't work anymore, as it depended on the search function.)

It used to be possible to give the bot a screenshot of the score screen, after which it would read the beatmap's name from the top of the screenshot and show you information about the score.

The mods and amounts of misses/100s/50s had to be specified manually.

Old screenshot:
![](screenshots/screenshot_command.png)

## Specifying mods

For commands that take mods, you can specify them with a plus followed by a combination of: `ez`, `hd`, `hr`, `ht`, `dt`, `nc`, `fl`, `nf`, `so`, `sd`, `pf`, `td`. For example, `+hddt` indicates Hidden + DoubleTime.

To indicate NoMod, write `+nm`.

You can mark mods as optional by adding a question mark after them. E.g. `+hd?dt` means "either +DT or +HDDT".

Also, because DoubleTime and Nightcore are basically the same mod, there is a special notation where you can specify either of them by writing `dt*` (`nc*` also works). E.g. `+hddt*` means "either +HDDT or +HDNC".

The same applies to SuddenDeath and Perfect: you can include both by writing `sd*` (or `pf*`).

Other characters are ignored, so `+hd,dt` is equivalent to `+hddt`. Just be careful not to include spaces: `+hd, dt` will only see Hidden.

To avoid spamming osu! with requests, you can't hav more than 8 mod combinations per command. E.g. `hd?hr?dt*fl?` would not work, because 2 options for HD (on/off), 2 for HR, 2 for DT/NC and 2 for FL produce 16 combinations in total.
