const discord = require("discord.js");
const osu = require("./osu");
const fs = require("fs");
const client = new discord.Client();

const adminID = process.env.ADMIN_ID;

var userContext = {};
try {
  userContext = JSON.parse(fs.readFileSync("userContext.json").toString());
} catch (error) {
  console.log(error);
}

var lastMapIDs = {};

client.on("ready", () => {
  console.log(`logged in as ${client.user.tag}`);
});

client.on("message", async (msg) => {
  msg.content = msg.content.replace(
    /https?:\/\/osu.ppy.sh\/(b|s)\/(\d+)\S*/g,
    "#$1$2#"
  );
  msg.content = msg.content.replace(
    /https?:\/\/osu.ppy.sh\/beatmaps\/(\d+)\S*/g,
    "#b$1#"
  );
  msg.content = msg.content.replace(
    /https?:\/\/osu.ppy.sh\/beatmapsets\/\d+#osu\/(\d+)\S*/g,
    "#b$1#"
  );
  msg.content = msg.content.replace(
    /https?:\/\/osu.ppy.sh\/beatmapsets\/(\d+)\S*/g,
    "#s$1#"
  );

  if (msg.content === "!>:C" || msg.content === "!>:С") {
    msg.channel.stopTyping();
    return;
  }
  if (msg.content.startsWith("!")) {
    console.debug(new Date(Date.now()).toLocaleTimeString(), msg.content);
    msg.channel.startTyping();

    let searchMatch = msg.content.match(
      /\bfind\s?\((([^,]*?)\s+-\s+)?(.+?)(\s*\[(.*)\])?(,\s*([^)]*))?\)/
    );
    if (searchMatch) {
      try {
        let foundID = await osu.find(
          searchMatch[2],
          searchMatch[3],
          searchMatch[5],
          searchMatch[7]
        );
        msg.content = msg.content.replace(searchMatch[0], `#b${foundID}#`);
      } catch (err) {
        if (err === "noBeatmap")
          msg.channel.send("Не удалось найти карту по такому запросу.");
        else if (err === "searchTimeout")
          msg.channel.send("Поиск карт не отвечает...");
        else {
          console.log(err);
          msg.channel.send(`Что-то пошло не так. <@${adminID}> оповещён.`);
        }
      }
    }

    if (msg.embeds.length > 0) {
      for (let emb of msg.embeds) {
        if (emb.type === "image") {
          try {
            let bid = await osu.getIDFromScreenshot(emb.url);
            msg.content = msg.content.replace(emb.url, `#b${bid}#`);
          } catch (err) {
            if (err === "timeout")
              msg.channel.send("Не удалось скачать скриншот.");
            else if (err === "searchTimeout")
              msg.channel.send("Поиск карт не отвечает...");
            else if (err === "noBeatmap")
              msg.channel.send("Карты по скриншоту не найдено.");
            else throw err;
          }
          break;
        }
      }
    } else if (msg.attachments.size > 0) {
      let att = msg.attachments.values().next().value;
      try {
        let bid = await osu.getIDFromScreenshot(att.url);
        let beginning = msg.content.match(/!\s*[a-z]*/i)[0];
        msg.content =
          beginning + ` #b${bid}# ` + msg.content.slice(beginning.length);
      } catch (err) {
        if (err === "timeout") msg.channel.send("Не удалось скачать скриншот.");
        else if (err === "searchTimeout")
          msg.channel.send("Поиск карт не отвечает...");
        else if (err === "noBeatmap")
          msg.channel.send("Карты по скриншоту не найдено.");
        else throw err;
      }
    }

    const match = msg.content.match(/!\s*([a-z]+|#.*?#)\s*(.*)/i);
    if (match === null) return;

    const cmd = match[1].toLowerCase();
    const args = match[2];
    let isUserFromContext =
      userContext[msg.author.id] && userContext[msg.author.id].osuName;
    let userFromContext = isUserFromContext
      ? userContext[msg.author.id].osuName
      : undefined;
    let beatmapFromContext = lastMapIDs[msg.channel.id];
    try {
      if (cmd == "u" || cmd == "user") {
        let user = args.trim() !== "" ? args.trim() : userFromContext;
        if (!user) throw "noUserContext";
        isUserFromContext = user == userFromContext;

        let [embed, text] = await osu.getUserEmbed(user);
        msg.channel.send(text, { embed: embed });
      } else if (cmd.startsWith("#b") || cmd.startsWith("#s")) {
        let id = cmd.match(/#(?:b|s)(\d+)#/)[1];
        let [embed, text, bid, graph] = await osu.getBeatmapEmbed(
          id,
          cmd[1],
          args
        );

        msg.channel.send(text, { embed: embed });
        lastMapIDs[msg.channel.id] = bid;
      } else if (cmd == "s" || cmd == "i" || cmd == "same" || cmd == "info") {
        let idMatch = args.match(/#(b|s)(\d+)#/);
        if (!idMatch && !beatmapFromContext) throw "noMapContext";

        let [embed, text, bid, graph] = idMatch
          ? await osu.getBeatmapEmbed(idMatch[2], idMatch[1], args)
          : await osu.getBeatmapEmbed(beatmapFromContext, "b", args);

        msg.channel.send(text, { embed: embed });
        lastMapIDs[msg.channel.id] = bid;
      } else if (cmd == "l" || cmd == "last" || cmd == "latest") {
        let user = args.trim() !== "" ? args.trim() : userFromContext;
        if (!user) throw "noUserContext";
        isUserFromContext = user == userFromContext;

        try {
          let [embed, text, bid] = await osu.getLatestPlayEmbed(user);
          msg.channel.send(text, { embed: embed });
          lastMapIDs[msg.channel.id] = bid;
        } catch (err) {
          if (err === "noPlays") {
            msg.channel.send(
              isUserFromContext
                ? `Вы за последнее время ничего не прошли.`
                : `Данный игрок за последнее время ничего не прошёл.`
            );
          } else throw err;
        }
      } else if (cmd == "f" || cmd == "fail" || cmd == "failed") {
        let user = args.trim() !== "" ? args.trim() : userFromContext;
        if (!user) throw "noUserContext";
        isUserFromContext = user == userFromContext;

        try {
          let [embed, text, bid] = await osu.getLatestFailedPlayEmbed(user);
          msg.channel.send(text, { embed: embed });
          lastMapIDs[msg.channel.id] = bid;
        } catch (err) {
          if (err === "noPlays") {
            msg.channel.send(
              isUserFromContext
                ? `Вы за последнее время ничего не проиграли.`
                : `Данный игрок за последнее время ничего не проиграл.`
            );
          } else throw err;
        }
      } else if (cmd == "b" || cmd == "best") {
        let user = args
          .replace(/\s*#(?:b|s)\d+#\s*/, " ")
          .replace(/(?:^|\s+)\+\S+(?:$|\s+)/, " ")
          .replace(/(?:^|\s+)\/\w+(?:$|\s+)/, " ")
          .trim();
        if (user === "") user = userFromContext;
        if (!user) throw "noUserContext";
        isUserFromContext = user == userFromContext;

        let idMatch = args.match(/#(b|s)(\d+)#/);
        if (!idMatch && !beatmapFromContext) throw "noMapContext";

        try {
          let [embed, text, bid] = idMatch
            ? await osu.getPersonalBestEmbed(user, idMatch[2], idMatch[1], args)
            : await osu.getPersonalBestEmbed(
                user,
                beatmapFromContext,
                "b",
                args
              );
          msg.channel.send(text, { embed: embed });
          lastMapIDs[msg.channel.id] = bid;
        } catch (err) {
          if (err === "noPlays") {
            msg.channel.send(
              isUserFromContext
                ? `Вы эту карту не проходили.`
                : `Данный игрок эту карту не проходил.`
            );
          } else throw err;
        }
      } else if (cmd == "t" || cmd == "top") {
        let count = args.match(/^(\d+)/);
        count = count ? parseInt(count[1]) : 3;
        if (count === 0) count = 3;
        if (count > 20) count = 20;

        let idMatch = args.match(/#(b|s)(\d+)#/);
        if (!idMatch && !beatmapFromContext) throw "noMapContext";

        try {
          let [embed, text, bid] = idMatch
            ? await osu.getMapTopEmbed(idMatch[2], idMatch[1], count, args)
            : await osu.getMapTopEmbed(beatmapFromContext, "b", count, args);
          msg.channel.send(text, { embed: embed });
          lastMapIDs[msg.channel.id] = bid;
        } catch (err) {
          if (err === "noPlays") {
            msg.channel.send(`Эту карту ещё никто не прошёл`);
          } else if (err === "noModPlays") {
            msg.channel.send(`С данными модами эту карту ещё никто не прошёл.`);
          } else throw err;
        }
      } else if (cmd == "test") {
        console.debug(msg);
      }

      msg.channel.stopTyping();
    } catch (err) {
      if (err === "osuTimeout") msg.channel.send(`Сервер osu! не отвечает...`);
      else if (err === "noUser")
        msg.channel.send(
          isUserFromContext
            ? `Я думал, что вы ${userFromContext}, но вас не существует?`
            : `Такого игрока не существует.`
        );
      else if (err === "noBeatmap")
        msg.channel.send(`Такой карты не существует.`);
      else if (err === "noLeaderboard")
        msg.channel.send(`У этой карты вообще нет таблицы рекордов.`);
      else if (err === "noBeatmapFile")
        msg.channel.send(`Не удалось скачать карту.`);
      else if (err === "noUserContext")
        msg.channel.send(`Я не знаю, кто вы. <@${adminID}> может помочь.`);
      else if (err === "noMapContext")
        msg.channel.send(`Я случайно забыл последнюю карту :thinking:`);
      else if (err === "tooManyMods")
        msg.channel.send(`Не более восьми комбинаций модов, пожалуйста.`);
      else if (err === "graphFailed")
        msg.channel.send(`Не удалось создать график.`);
      else {
        console.log(err);
        msg.channel.send(`Что-то пошло не так. <@${adminID}> оповещён.`);
      }

      msg.channel.stopTyping();
    }
    msg.channel.stopTyping();
  }
});

client.login(process.env.DISCORD_TOKEN);
