module.exports.r = (num) => {
    if(typeof num === 'string')
        num = parseFloat(num);
    return +num.toFixed(2);
};

module.exports.t = (num) => {
    if(typeof num === 'string')
        num = parseFloat(num);
    return num.toLocaleString('en-US');
};

module.exports.i = (num) => {
    return num.toString().replace('Infinity', '∞');
};

module.exports.n = (num, base, e1, e24, e50) => {
    var lastTwo = num % 100;
    if(lastTwo >= 10 && lastTwo <= 20)
        return num + ' ' + base + e50;
    else if(num % 10 == 1)
        return num + ' ' + base + e1;
    else if(num % 10 >= 2 && num % 10 <= 4)
        return num + ' ' + base + e24;
    else
        return num + ' ' + base + e50;
};

module.exports.rt = (num) => {return module.exports.t(module.exports.r(num));}

module.exports.len = (sec) => {
    const h = Math.floor(sec/3600);
    const m = Math.floor((sec%3600)/60);
    const s = Math.round(sec%60);

    return (h ? h + ':' : '') + (h && m < 10 ? '0' : '') + m + ':' + (s < 10 ? '0' : '') + s + '';
};

module.exports.big = (num, threshold, infThreshold) => {
    if(Math.abs(num) < threshold)
        return num.toString();

    if(Math.abs(num) >= infThreshold)
        return (num < 0) ? '-∞' : '∞';

    var sign = '';
    if(num < 0) {sign = '-'; num = -num;}
    var power = Math.floor(Math.log10(num));
    var base = Math.round(num / Math.pow(10, power));
    if(base == 10) {base = 1; power++;}
    return sign + base + 'e' + power;
};

module.exports.getBPMInfo = (map) => {
    var tps = [];
    var startTime = map.objects[0].time, endTime = map.objects[map.objects.length-1].time;
    for(let tp of map.timing_points) {
        if(tp.change) {
            tps.push({bpm: 60000 / tp.ms_per_beat, time: tp.time});
        }
    }
    var startBPM = tps[0].bpm, endBPM = startBPM;
    for(let tp of tps) {
        if(tp.time <= startTime)
            startBPM = tp.bpm;
        if(tp.time < endTime)
            endBPM = tp.bpm;
        else break;
    }
    tps = [{bpm: startBPM, time: startTime},
           ...tps.filter(tp => (tp.time > startTime && tp.time < endTime)),
           {bpm: endBPM, time: endTime}];

    var maxBPM = -Infinity, minBPM = Infinity;
    var durations = {};
    for(let i = 0; i < tps.length-1; i++) {
        if(tps[i].bpm > maxBPM) maxBPM = tps[i].bpm;
        if(tps[i].bpm < minBPM) minBPM = tps[i].bpm;

        durations[tps[i].bpm] = (durations[tps[i].bpm] || 0) + tps[i+1].time - tps[i].time;
    } 
    
    const midTime = (endTime - startTime) / 2;
    const sortedBPMs = Object.keys(durations).sort((a,b) => a-b);
    
    var meanBPM = 0, time = 0;
    for(let bpm of sortedBPMs) {
        time += durations[bpm];
        if(time > midTime) {
            meanBPM = bpm;
            break;
        }
    }
    if(meanBPM === 0)
        meanBPM = sortedBPMs[sortedBPMs.length-1];

    return {min: {ht: module.exports.big(module.exports.r(minBPM*0.75), 100000, 1e200),
                  nm: module.exports.big(module.exports.r(minBPM), 100000, 1e200),
                  dt: module.exports.big(module.exports.r(minBPM*1.5), 100000, 1e200)},
            mean: {ht: module.exports.big(module.exports.r(meanBPM*0.75), 100000, 1e200),
                   nm: module.exports.big(module.exports.r(meanBPM), 100000, 1e200),
                   dt: module.exports.big(module.exports.r(meanBPM*1.5), 100000, 1e200)},
            max: {ht: module.exports.big(module.exports.r(maxBPM*0.75), 100000, 1e200),
                  nm: module.exports.big(module.exports.r(maxBPM), 100000, 1e200),
                  dt: module.exports.big(module.exports.r(maxBPM*1.5), 100000, 1e200)}};
};

module.exports.esc = (str) => {
    return str.replace(/([\\\*\`\_\~`])/g, '\\$1');
};

function stringSim(have, want) {
    if(have.length === 0) return 0;

    have = have.toLowerCase()
    want = want.toLowerCase()
    let wWant = want.split(/\s+/)

    let found = 0
    for(let w of wWant) {
        if(have.match(w)) found++;
    }

    let res1 = found / wWant.length;

    let rest = have;
    wWant = wWant.sort((a,b) => b.length - a.length);
    for(let w of wWant) {
        rest = rest.replace(w, '');
    }
    rest = rest.replace(/\s+/g, '');
    
    let res2 = 1 - rest.length/have.length;

    return Math.sqrt(res1*res2);
}

function looseSim(have, want) {
    let matched = 0;
    for(let i=0; i<want.length; i++) {
        if(want[i] === have[i])
            matched++;
    }
    return matched / want.length;
}

module.exports.queryMatch = (map, query) => {
    let artistM = (!query[0]) ? 1 : stringSim(map[0], query[0]);
    let titleM = (!query[1]) ? 1 : stringSim(map[1], query[1]);
    let diffM = (!query[2]) ? 1 : stringSim(map[2], query[2]);
    let mapperM = (!query[3]) ? 1 : looseSim(map[3], query[3]);

    let res = Math.pow(artistM*titleM*diffM*mapperM, 0.25);
    if(map[4] > 0) res *= 2;

    return res;
};