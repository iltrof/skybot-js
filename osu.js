const axios = require("axios");
const axiosRetry = require("axios-retry");
const cache = require("./cache");
const fs = require("fs");
const oppai = require("ojsama");
const osuMods = require("./osuMods");
const _u = require("./util");
const timeago = require("timeago.js");
timeago.register("ru", require("./node_modules/timeago.js/lib/lang/ru"));
const FuzzySet = require("fuzzyset.js");
const { execSync } = require("child_process");
const cloudinary = require("cloudinary");
cloudinary.config({
  cloud_name: "iltrof",
  api_key: process.env.CLOUDINARY_KEY,
  api_secret: process.env.CLOUDINARY_SECRET,
});
const sharp = require("sharp");
const crypto = require("crypto");

const osuClient = axios.create({
  baseURL: "https://osu.ppy.sh/api/",
  timeout: 3000,
  headers: { "user-agent": `iltrof's Discord bot (iltrofx@gmail.com)` },
});

axiosRetry(axios, { retries: 3 });
axiosRetry(osuClient, { retries: 3 });

const apiKey = process.env.OSU_TOKEN;
const skyColor = 0x00a5f2;
const rankEmojis = {
  A: "<:osuA:346287838922539009>",
  B: "<:osuB:346288149493972994>",
  C: "<:osuC:346288155147632642>",
  D: "<:osuD:346288161674100737>",
  F: "<:osuF:361342063071068160>",
  S: "<:osuS:346288161741078530>",
  SH: "<:osuSH:346288159740526594>",
  X: "<:osuX:346288162773008395>",
  XH: "<:osuXH:346288162458435584>",
};

async function timeoutRetry(func, tries = 5, errCode = "osuTimeout") {
  var done = false,
    res;
  while (!done && tries--) {
    try {
      res = await func();
      done = true;
    } catch (err) {
      if (err.code !== "ECONNABORTED") throw err;
      else console.debug(errCode, `${tries} tries left`);
    }
  }
  if (!done) throw errCode;
  else return res;
}

async function getUser(name) {
  const response = await timeoutRetry(
    osuClient.get.bind(
      osuClient.get,
      `get_user?k=${apiKey}&u=${name}&type=string`
    )
  );
  console.debug(`get_user(${name}) returned`, response.data);
  if (response.data.length === 0) throw "noUser";
  return response.data[0];
}

async function getUserAvatar(id) {
  const cached = cache.get("avatars", id);
  if (cached) return cached;

  let response;
  try {
    response = await timeoutRetry(
      axios.get.bind(axios.get, `https://osu.ppy.sh/users/${id}/card`, {
        timeout: 3000,
        headers: { "user-agent": `iltrof's Discord bot (iltrofx@gmail.com)` },
      }),
      3
    );
  } catch (err) {
    if (err === "osuTimeout") {
      console.log(`Таймаут при считывании аватарки u/${id}`);
      return "";
    } else throw err;
  }

  const url = response.data.avatar_url || "";
  if (url !== "") {
    cache.set("avatars", id, response.data.avatar_url);
  }
  return url;
}

async function getBasicUserInfo(name) {
  name = name.toLowerCase();
  const cached = cache.get("basicUserInfo", name);
  if (cached) return cached;

  const user = await getUser(name);
  const res = {
    name: user.username,
    country: user.country.toLowerCase(),
    id: user.user_id,
  };
  cache.set("basicUserInfo", name, res);
  return res;
}

async function getUserLatest(name, fail = false) {
  const response = await timeoutRetry(
    osuClient.get.bind(
      osuClient.get,
      `get_user_recent?k=${apiKey}&u=${name}&type=string&m=0&limit=50`
    )
  );
  console.debug(`get_user_recent(${name}) returned`, response.data);

  var plays;
  if (fail) plays = response.data.filter((p) => p.rank == "F");
  else plays = response.data.filter((p) => p.rank != "F");

  if (plays.length === 0) throw "noPlays";

  plays[0].pp = null;
  if (fail) return plays[0];
  try {
    let best = await timeoutRetry(
      osuClient.get.bind(
        osuClient.get,
        `get_scores?k=${apiKey}&u=${name}&b=${plays[0].beatmap_id}&type=string&m=0`
      ),
      3
    );
    best = best.data.filter((p) => p.enabled_mods == plays[0].enabled_mods)[0];
    if (
      parseFloat(best.pp) &&
      Date.parse(best.date) == Date.parse(plays[0].date)
    )
      plays[0].pp = best.pp;
  } catch (err) {
    console.debug(err);
  }
  return plays[0];
}

function calcAcc(c300, c100, c50, cmiss) {
  return (
    (c300 * 300 + c100 * 100 + c50 * 50) / ((c300 + c100 + c50 + cmiss) * 300)
  );
}

async function getPersonalBest(name, id, parsedMap, mods, sort = "score") {
  sort = sort.toLowerCase();
  const response = await timeoutRetry(
    osuClient.get.bind(
      osuClient.get,
      `get_scores?k=${apiKey}&u=${name}&b=${id}&type=string&m=0`
    )
  );
  console.debug(`get_scores(${name}, ${id}) returned`, response.data);

  var plays = response.data;
  if (plays.length === 0) throw "noPlays";
  if (mods !== null)
    plays = response.data.filter((p) =>
      mods.includes(parseInt(p.enabled_mods))
    );
  if (plays.length === 0) plays = response.data;

  if (sort === "pp") {
    for (let i = 0; i < plays.length; i++) {
      if (parseFloat(plays[i].pp)) continue;

      let stars = new oppai.diff().calc({
        map: parsedMap,
        mods: parseInt(plays[i].enabled_mods),
      });
      let pp = oppai.ppv2({
        stars: stars,
        combo: parseInt(plays[i].maxcombo),
        n100: parseInt(plays[i].count100),
        n50: parseInt(plays[i].count50),
        nmiss: parseInt(plays[i].countmiss),
      });
      plays[i].pp = pp.total;
    }

    plays = plays.sort((p1, p2) => p2.pp - p1.pp);
  } else if (sort === "time" || sort === "date") {
    plays = plays.sort((p1, p2) => Date.parse(p2.date) - Date.parse(p1.date));
  } else if (sort === "acc") {
    plays = plays.sort((p1, p2) => calcAcc(p2) - calcAcc(p1));
  } else {
    plays = plays.sort((p1, p2) => p2.score - p1.score);
  }

  if (!parseFloat(plays[0].pp)) {
    let stars = new oppai.diff().calc({
      map: parsedMap,
      mods: parseInt(plays[0].enabled_mods),
    });
    let pp = oppai.ppv2({
      stars: stars,
      combo: parseInt(plays[0].maxcombo),
      n100: parseInt(plays[0].count100),
      n50: parseInt(plays[0].count50),
      nmiss: parseInt(plays[0].countmiss),
    });
    plays[0].pp = pp.total;
  }

  return plays[0];
}

async function getMapTop(id, parsedMap, mods, count) {
  let plays = [];
  if (mods === null)
    plays = (
      await timeoutRetry(
        osuClient.get.bind(
          osuClient.get,
          `get_scores?k=${apiKey}&b=${id}&m=0&limit=${count}`
        )
      )
    ).data;
  else {
    for (let m of mods) {
      let res = (
        await timeoutRetry(
          osuClient.get.bind(
            osuClient.get,
            `get_scores?k=${apiKey}&b=${id}&mods=${m}&m=0&limit=${count}`
          )
        )
      ).data;
      plays = [...plays, ...res];
    }
  }
  console.debug(`get_scores(${id}) returned:`, plays);
  if (plays.length === 0) throw mods ? "noModPlays" : "noPlays";

  let uniquePlays = [];
  let names = [...new Set(plays.map((p) => p.username))];
  for (let name of names) {
    uniquePlays.push(
      plays
        .filter((p) => p.username === name)
        .sort((p1, p2) => p2.score - p1.score)[0]
    );
  }
  uniquePlays = uniquePlays.sort((p1, p2) => p2.score - p1.score);

  for (let i = 0; i < Math.min(count, uniquePlays.length, 5); i++) {
    if (parseFloat(uniquePlays[i].pp)) continue;

    let stars = new oppai.diff().calc({
      map: parsedMap,
      mods: parseInt(uniquePlays[i].enabled_mods),
    });
    let pp = oppai.ppv2({
      stars: stars,
      combo: parseInt(uniquePlays[i].maxcombo),
      n100: parseInt(uniquePlays[i].count100),
      n50: parseInt(uniquePlays[i].count50),
      nmiss: parseInt(uniquePlays[i].countmiss),
    });
    uniquePlays[i].pp = pp.total;
  }

  return uniquePlays.slice(0, count);
}

async function getBeatmap(id) {
  try {
    const bm = JSON.parse(
      fs.readFileSync(`cache/beatmaps/b${id}.json`).toString()
    );
    const bmosu = fs.readFileSync(`cache/beatmaps/b${id}.osu`).toString();
    bm["osuFile"] = bmosu;
    console.debug(`Loaded b${id} from cache.`);

    return bm;
  } catch (err) {
    console.debug(err);
  }

  var response = await timeoutRetry(
    osuClient.get.bind(osuClient.get, `get_beatmaps?k=${apiKey}&b=${id}&m=0`)
  );
  console.debug(`get_beatmaps(b${id}) returned`, response.data);
  if (response.data.length === 0) {
    let r = await timeoutRetry(
      axios.get.bind(
        axios.get,
        `https://bloodcat.com/osu/?q=${id}&c=b&mod=json`,
        {
          timeout: 3000,
          headers: { "user-agent": `iltrof's Discord bot (iltrofx@gmail.com)` },
        }
      )
    );

    if (r.data.length === 0) throw "noBeatmap";

    let set = r.data[0];
    let bm = set.beatmaps.filter((m) => m.id == id)[0];
    response.data[0] = {
      beatmapset_id: set.id,
      beatmap_id: bm.id,
      approved: set.status,
      total_length: bm.length,
      hit_length: bm.length,
      version: bm.name,
      file_md5: bm.hash_md5,
      diff_size: bm.cs,
      diff_overall: bm.od,
      diff_approach: bm.ar,
      diff_drain: bm.hp,
      mode: bm.mode,
      approved_date: set.rankedAt,
      last_update: set.rankedAt,
      artist: set.artist,
      title: set.title,
      creator: set.creator,
      bpm: bm.bpm,
      source: set.source,
      tags: set.tags,
      genre_id: set.genreId,
      language_id: set.languageId,
      favourite_count: null,
      playcount: null,
      passcount: null,
      max_combo: null,
      difficultyrating: null,
      bloodcatOnly: true,
    };
  }

  var osuFile;
  try {
    osuFile = await timeoutRetry(
      axios.get.bind(axios.get, `https://osu.ppy.sh/osu/${id}`, {
        timeout: 3000,
        headers: { "user-agent": `iltrof's Discord bot (iltrofx@gmail.com)` },
      })
    );

    if (osuFile.data.length === 0) throw "none";
  } catch (err) {
    console.log(err);
    osuFile = await timeoutRetry(
      axios.get.bind(axios.get, `https://bloodcat.com/osu/b/${id}`, {
        timeout: 3000,
        headers: { "user-agent": `iltrof's Discord bot (iltrofx@gmail.com)` },
      })
    );
  }

  if (!osuFile.data) throw "noBeatmapFile";

  fs.writeFile(
    `cache/beatmaps/b${id}.json`,
    JSON.stringify(response.data[0]),
    (err) => {
      if (err) console.debug(err);
      else console.debug(`Saved b${id} to cache.`);
    }
  );
  fs.writeFile(`cache/beatmaps/b${id}.osu`, osuFile.data, (err) => {
    if (err) console.debug(err);
    else console.debug(`Saved b${id}.osu to cache.`);
  });

  response.data[0]["osuFile"] = osuFile.data;
  return response.data[0];
}

async function getBeatmapS(sid) {
  const cached = cache.get("setToMapIDs", sid);
  if (cached) return await getBeatmap(cached);

  const response = await timeoutRetry(
    osuClient.get.bind(osuClient.get, `get_beatmaps?k=${apiKey}&s=${sid}&m=0`)
  );
  console.debug(`get_beatmaps(s${sid}) returned`, response.data);
  if (response.data.length === 0) throw "noBeatmap";
  const diffs = response.data.sort(
    (a, b) => b.difficultyrating - a.difficultyrating
  );
  cache.set("setToMapIDs", sid, diffs[0].beatmap_id);

  try {
    const bm = JSON.parse(
      fs.readFileSync(`cache/beatmaps/b${diffs[0].beatmap_id}.json`).toString()
    );
    const bmosu = fs
      .readFileSync(`cache/beatmaps/b${diffs[0].beatmap_id}.osu`)
      .toString();
    bm["osuFile"] = bmosu;
    console.debug(`Loaded b${diffs[0].beatmap_id} from cache.`);

    return bm;
  } catch (err) {
    console.debug(err);
  }

  const osuFile = await timeoutRetry(
    axios.get.bind(axios.get, `https://osu.ppy.sh/osu/${diffs[0].beatmap_id}`, {
      timeout: 3000,
    })
  );
  if (!osuFile.data) throw "noBeatmapFile";

  fs.writeFile(
    `cache/beatmaps/b${diffs[0].beatmap_id}.json`,
    JSON.stringify(diffs[0]),
    (err) => {
      if (err) console.debug(err);
      else console.debug(`Saved b${diffs[0].beatmap_id} to cache.`);
    }
  );
  fs.writeFile(
    `cache/beatmaps/b${diffs[0].beatmap_id}.osu`,
    osuFile.data,
    (err) => {
      if (err) console.debug(err);
      else console.debug(`Saved b${diffs[0].beatmap_id}.osu to cache.`);
    }
  );

  diffs[0]["osuFile"] = osuFile.data;
  return diffs[0];
}

module.exports.getUserEmbed = async (name) => {
  const user = await getUser(name);
  return [
    {
      color: skyColor,
      title: `:flag_${user.country.toLowerCase()}: ${_u.esc(user.username)}`,
      description:
        `**#${_u.t(user.pp_rank)}** (#${_u.t(
          user.pp_country_rank
        )}) **· ${_u.rt(user.pp_raw)}pp**\n` +
        `${_u.r(user.accuracy)}% **·** ${_u.t(
          user.playcount
        )} plays **·** lvl${Math.floor(user.level)}\n` +
        `[Профиль](https://osu.ppy.sh/u/${user.user_id}) ` +
        `**·** [Статистика](https://osudaily.net/profile.php?u=${encodeURI(
          user.username
        )})`,
      thumbnail: { url: await getUserAvatar(user.user_id) },
    },
    "",
  ];
};

async function makeBeatmapInfo(bm, mods, stars) {
  const bpm = _u.getBPMInfo(stars.map);
  const stats = new oppai.std_beatmap_stats(stars.map).with_mods(mods);
  const mapBPM = _u.big(_u.r(bpm.mean.nm * stats.speed_mul), 100000, 1e200);

  return [
    `https://b.ppy.sh/thumb/${bm.beatmapset_id}.jpg`,
    `${mods ? `(+${osuMods.toString(mods)}) ` : ""}${_u.esc(
      bm.artist
    )} — ${_u.esc(bm.title)} [${_u.esc(bm.version)}]`,
    `CS${_u.r(stats.cs)} **·** HP${_u.r(stats.hp)} **·** OD${_u.r(
      stats.od
    )} **·** AR**${_u.r(stats.ar)}**\n` +
      `**${mapBPM}** BPM[⁺](http://a.a/ ` +
      `"HT: ${bpm.min.ht}—${bpm.max.ht} (${bpm.mean.ht})\n` +
      `NM: ${bpm.min.nm}—${bpm.max.nm} (${bpm.mean.nm})\n` +
      `DT: ${bpm.min.dt}—${bpm.max.dt} (${bpm.mean.dt})") ` +
      `**·** ${_u.len(bm.total_length / stats.speed_mul)} **·** ${_u.t(
        stars.map.max_combo()
      )}x[⁺](http://a.a/ ` +
      `"${_u.n(stars.map.ncircles, "круж", "ок", "ка", "ков")}\n` +
      `${_u.n(stars.map.nsliders, "слайдер", "", "а", "ов")}\n` +
      `${_u.n(stars.map.nspinners, "спиннер", "", "а", "ов")}") **·** ` +
      `[★](http://a.a/ "Прицел — ★${_u.r(stars.aim)}\nСкорость — ★${_u.r(
        stars.speed
      )}")**${_u.r(stars.total)}**\n` +
      (bm.bloodcatOnly
        ? `Карта`
        : `[Карта](https://osu.ppy.sh/b/${bm.beatmap_id})`) +
      ` **·** [⇓⇓⇓]` +
      (bm.bloodcatOnly
        ? `(http://bloodcat.com/osu/s/${bm.beatmapset_id} "Скачать") `
        : `(https://osu.ppy.sh/d/${bm.beatmapset_id}n "Скачать") `) +
      `**·** [Превью](http://bloodcat.com/osu/preview.html#${bm.beatmap_id}) **·** ${bm.creator}`,
  ];
}

async function makeBeatmapGraph(bm, stars) {
  const cached = cache.get("graphURLs", bm.beatmap_id);
  if (cached) return cached;

  var fname = `graphs/${bm.beatmap_id}`;
  var done = false;
  var success = false;

  var data = "";
  var maxv = 0;
  for (let i = 0; i < stars.starList[0].length; i++) {
    let a = stars.starList[0][i];
    let b = stars.starList[1][i];
    let v = Math.sqrt((a + b + Math.abs(a - b) * 0.5) * 0.0675);
    if (v > maxv) maxv = v;

    let t = (400 * (i + 1)) / 1000;
    data += `${t} ${v}\n`;
  }
  var maxt = (400 * stars.starList[0].length) / 1000;

  try {
    fs.writeFileSync(`${fname}.txt`, data);
  } catch (err) {
    console.debug(err);
    throw "graphFailed";
  }

  try {
    execSync(
      `"C:/Program Files/gnuplot/bin/gnuplot.exe" -e "` +
        `set terminal pngcairo enhanced size 400,200 background rgb '#32363b'; ` +
        `set output '${fname}.png'; ` +
        `set style line 1 lc rgb 'cyan' lw 2; ` +
        `unset border; ` +
        `set style data lines; ` +
        `set bmargin 0.5; set rmargin 1; set lmargin 1; set tmargin 0.5; ` +
        `unset tics; ` +
        `set nokey; ` +
        `set timefmt '%s'; ` +
        `set xdata time; ` +
        `plot [0:${maxt}][0:${maxv}] '${fname}.txt' using 1:2 smooth acsplines ls 1` +
        `"`
    );
  } catch (err) {
    console.debug(err);
    throw "graphFailed";
  }

  if (fs.existsSync(`${fname}.png`)) {
    let res = await cloudinary.uploader.upload(`${fname}.png`);
    cache.set("graphURLs", bm.beatmap_id, res.url);
    return cache.get("graphURLs", bm.beatmap_id);
  } else throw "graphFailed";
}

module.exports.getBeatmapEmbed = async (id, type, args) => {
  var bm;
  if (type == "b") bm = await getBeatmap(id);
  else if (type == "s") bm = await getBeatmapS(id);

  var graphMatch = args.match(/\/(graph(only)?|g(o)?)\b/i);
  var makeGraph = graphMatch !== null;
  var graphOnly = graphMatch
    ? graphMatch[2] || graphMatch[3]
      ? true
      : false
    : false;

  var combo =
    args.match(/(?:^|\s)(?:(\d+)[xх]|[xх](\d+))(?:$|\s)/) || undefined;
  if (combo) combo = parseInt(combo[1] || combo[2]) || undefined;
  var n100 = args.match(/(?:^|\s)(\d+)[xх]100(?:$|\s)/) || undefined;
  if (n100) n100 = parseInt(n100[1]) || undefined;
  var n50 = args.match(/(?:^|\s)(\d+)[xх]50(?:$|\s)/) || undefined;
  if (n50) n50 = parseInt(n50[1]) || undefined;
  var nmiss = args.match(/(?:^|\s)(\d+)[mм](?:$|\s)/) || undefined;
  if (nmiss) nmiss = parseInt(nmiss[1]) || undefined;
  var acc = args.match(/(?:^|\s)([\d.]+)%(?:$|\s)/) || undefined;
  if (acc) acc = parseFloat(acc[1]) || undefined;
  var mods = args.match(/(?:^|\s)\+(\S+)(?:$|\s)/) || [];
  mods = osuMods.parseModToken(mods[1]);

  const parser = new oppai.parser();
  parser.feed(bm.osuFile);

  if (makeGraph && graphOnly) {
    let stars = new oppai.diff().calc({ map: parser.map, mods: mods });
    let graph = await makeBeatmapGraph(bm, stars);

    return [
      { color: skyColor, image: { url: graph } },
      "",
      bm.beatmap_id,
      graph,
    ];
  } else if (mods.length <= 1) {
    mods = mods[0] || 0;

    let stars = new oppai.diff().calc({ map: parser.map, mods: mods });
    let ppSS = oppai.ppv2({ stars: stars });
    let pp;
    try {
      pp = oppai.ppv2({
        stars: stars,
        combo: combo,
        n100: n100,
        n50: n50,
        nmiss: nmiss,
        acc_percent: acc,
      });
    } catch (error) {
      pp = null;
    }

    let [thumb, title, desc] = await makeBeatmapInfo(bm, mods, stars);
    if (makeGraph) var graph = await makeBeatmapGraph(bm, stars);
    return [
      {
        color: skyColor,
        thumbnail: { url: thumb },
        image: makeGraph ? { url: graph } : undefined,
        title: title,
        description:
          desc +
          `\n\nSS — ${_u.r(ppSS.total)}pp` +
          (pp && pp.total != ppSS.total
            ? ` **·** ${_u.r(100 * pp.computed_accuracy.value())}% ` +
              `${combo < parser.map.max_combo() ? `${combo}x ` : ``}` +
              `${nmiss ? nmiss + "m " : ""}— ${_u.r(pp.total)}pp`
            : ``),
      },
      pp === null ? "С этими параметрами что-то было не так." : "",
      bm.beatmap_id,
      makeGraph ? graph : null,
    ];
  } else {
    let commonMods = osuMods.getCommonMods(mods);
    let commonStars = new oppai.diff().calc({
      map: parser.map,
      mods: commonMods,
    });

    let pp = [];
    for (let m of mods) {
      let stars = new oppai.diff().calc({ map: parser.map, mods: m });
      pp.push({ mods: m, value: oppai.ppv2({ stars: stars }).total });
    }
    pp = pp.sort((a, b) => a.value - b.value);

    let res = "";
    for (let i = 0; i < pp.length; i++) {
      res += `${osuMods.toString(pp[i].mods)} — ${_u.r(pp[i].value)}pp`;
      if (i % 2 == 0 && i != pp.length - 1) res += " **·** ";
      else if (i % 2 == 1) res += "\n";
    }

    let [thumb, title, desc] = await makeBeatmapInfo(
      bm,
      commonMods,
      commonStars
    );
    if (makeGraph) var graph = await makeBeatmapGraph(bm, stars);
    return [
      {
        color: skyColor,
        thumbnail: { url: thumb },
        image: makeGraph ? { url: graph } : undefined,
        title: title,
        description: desc + "\n\n" + res,
      },
      "",
      bm.beatmap_id,
      makeGraph ? graph : null,
    ];
  }
};

module.exports.getLatestPlayEmbed = async (name) => {
  const userInfo = await getBasicUserInfo(name);
  const play = await getUserLatest(name, false);
  const bm = await getBeatmap(play.beatmap_id);

  const parser = new oppai.parser();
  parser.feed(bm.osuFile);
  const stars = new oppai.diff().calc({
    map: parser.map,
    mods: parseInt(play.enabled_mods),
  });

  const pp = oppai.ppv2({
    stars: stars,
    combo: parseInt(play.maxcombo),
    n100: parseInt(play.count100),
    n50: parseInt(play.count50),
    nmiss: parseInt(play.countmiss),
  });
  var ppFC = undefined;
  if (play.countmiss > 0 || play.maxcombo < parser.map.max_combo() - 20) {
    let minSBs = Math.ceil(parser.map.max_combo() / play.maxcombo) - 1;
    minSBs = Math.max(minSBs - play.countmiss, 0);

    let new50 = play.count50;
    if (minSBs >= new50) {
      minSBs -= new50;
      new50 = 0;
    } else {
      new50 -= minSBs;
      minSBs = 0;
    }
    let new100 = play.count100 - minSBs;

    ppFC = oppai.ppv2({ stars: stars, n100: new100, n50: new50 });
  }

  const [thumb, title, desc] = await makeBeatmapInfo(
    bm,
    parseInt(play.enabled_mods),
    stars
  );
  const ppStyle = bm.approved > 2 ? "~~" : "";
  return [
    {
      color: skyColor,
      thumbnail: { url: thumb },
      title: title,
      description: desc,
      fields: [
        {
          name:
            `:flag_${userInfo.country}: ${_u.esc(userInfo.name)} ` +
            `${
              parseInt(play.enabled_mods)
                ? `+${osuMods.toString(parseInt(play.enabled_mods))} `
                : ""
            }` +
            `(${timeago.format(play.date + " +0000", "en-US")}) ${
              rankEmojis[play.rank]
            }`,
          value:
            `${_u.t(play.score)} **· ${_u.r(
              100 * pp.computed_accuracy.value()
            )}%**` +
            `${
              ppFC ? ` (${_u.r(100 * ppFC.computed_accuracy.value())}%)` : ""
            } **·** ` +
            `${parseInt(play.perfect) ? "FC" : play.maxcombo + "x"}\n` +
            `${play.count300} ⁄ ${play.count100} ⁄ ${play.count50} ⁄ ${play.countmiss} **·** ` +
            `${ppStyle}**${_u.r(parseFloat(play.pp) || pp.total)}pp**${
              ppFC ? ` (${_u.r(ppFC.total)})` : ""
            }${ppStyle}`,
        },
      ],
    },
    "",
    play.beatmap_id,
  ];
};

module.exports.getLatestFailedPlayEmbed = async (name) => {
  const userInfo = await getBasicUserInfo(name);
  const play = await getUserLatest(name, true);
  const bm = await getBeatmap(play.beatmap_id);

  const parser = new oppai.parser();
  parser.feed(bm.osuFile);
  const stars = new oppai.diff().calc({
    map: parser.map,
    mods: parseInt(play.enabled_mods),
  });

  const ppFC = oppai.ppv2({
    stars: stars,
    n100: parseInt(play.count100),
    n50: parseInt(play.count50),
  });
  const nObj =
    +play.count300 + +play.count100 + +play.count50 + +play.countmiss;
  var completion =
    (100 * (parser.map.objects[nObj - 1].time - parser.map.objects[0].time)) /
    (parser.map.objects[parser.map.objects.length - 1].time -
      parser.map.objects[0].time);
  if (completion > 99.99) completion = 99.99;
  const acc =
    (100 * (play.count300 * 300 + play.count100 * 100 + play.count50 * 50)) /
    (nObj * 300);

  const [thumb, title, desc] = await makeBeatmapInfo(
    bm,
    parseInt(play.enabled_mods),
    stars
  );
  const ppStyle = bm.approved > 2 ? "~~" : "";
  return [
    {
      color: skyColor,
      thumbnail: { url: thumb },
      title: title,
      description: desc,
      fields: [
        {
          name:
            `:flag_${userInfo.country}: ${_u.esc(userInfo.name)} ` +
            `${
              parseInt(play.enabled_mods)
                ? `+${osuMods.toString(parseInt(play.enabled_mods))} `
                : ""
            }` +
            `(${timeago.format(play.date + " +0000", "en-US")}) ${
              rankEmojis[play.rank]
            }`,
          value:
            `${_u.t(play.score)} **· ${_u.r(acc)}% ·** ` +
            `${play.maxcombo + "x"} **· ${_u.r(completion)}%** карты\n` +
            `${play.count300} ⁄ ${play.count100} ⁄ ${play.count50} ⁄ ${play.countmiss} **·** ` +
            `${_u.r(
              100 * ppFC.computed_accuracy.value()
            )}% FC — ${ppStyle}${_u.r(ppFC.total)}pp${ppStyle}`,
        },
      ],
    },
    "",
    play.beatmap_id,
  ];
};

module.exports.getPersonalBestEmbed = async (name, id, type, args) => {
  const userInfo = await getBasicUserInfo(name);

  var bm = undefined;
  if (type == "b") bm = await getBeatmap(id);
  else if (type == "s") bm = await getBeatmapS(id);
  if (bm.approved <= 0) throw "noLeaderboard";

  const parser = new oppai.parser();
  parser.feed(bm.osuFile);

  var mods = args.match(/(?:^|\s)\+(\S+)(?:$|\s)/) || [];
  let filterMods = mods.length !== 0;
  mods = osuMods.parseModToken(mods[1]);

  var sort = args.match(/(?:^|\s)\/(\w+)(?:$|\s)/);
  sort = sort ? sort[1] : "score";
  var pb = await getPersonalBest(
    name,
    bm.beatmap_id,
    parser.map,
    filterMods ? mods : null,
    sort
  );

  const stars = new oppai.diff().calc({
    map: parser.map,
    mods: parseInt(pb.enabled_mods),
  });
  const acc = new oppai.std_accuracy({
    n300: parseInt(pb.count300),
    n100: parseInt(pb.count100),
    n50: parseInt(pb.count50),
    nmiss: parseInt(pb.countmiss),
  });
  var ppFC = undefined;
  if (pb.countmiss > 0 || pb.maxcombo < parser.map.max_combo() - 20) {
    let minSBs = Math.ceil(parser.map.max_combo() / pb.maxcombo) - 1;
    minSBs = Math.max(minSBs - pb.countmiss, 0);

    let new50 = pb.count50;
    if (minSBs >= new50) {
      minSBs -= new50;
      new50 = 0;
    } else {
      new50 -= minSBs;
      minSBs = 0;
    }
    let new100 = pb.count100 - minSBs;

    ppFC = oppai.ppv2({ stars: stars, n100: new100, n50: new50 });
  }

  const [thumb, title, desc] = await makeBeatmapInfo(
    bm,
    parseInt(pb.enabled_mods),
    stars
  );
  const ppStyle = bm.approved > 2 ? "~~" : "";
  return [
    {
      color: skyColor,
      thumbnail: { url: thumb },
      title: title,
      description: desc,
      fields: [
        {
          name:
            `:flag_${userInfo.country}: ${_u.esc(userInfo.name)} ` +
            `${
              parseInt(pb.enabled_mods)
                ? `+${osuMods.toString(parseInt(pb.enabled_mods))} `
                : ""
            }` +
            `(${timeago.format(pb.date + " +0000", "en-US")}) ${
              rankEmojis[pb.rank]
            }`,
          value:
            `${_u.t(pb.score)} **· ${_u.r(100 * acc.value())}%**` +
            `${
              ppFC ? ` (${_u.r(100 * ppFC.computed_accuracy.value())}%)` : ""
            } **·** ` +
            `${parseInt(pb.perfect) ? "FC" : pb.maxcombo + "x"}\n` +
            `${pb.count300} ⁄ ${pb.count100} ⁄ ${pb.count50} ⁄ ${pb.countmiss} **·** ` +
            `${ppStyle}**${_u.r(parseFloat(pb.pp))}pp**${
              ppFC ? ` (${_u.r(ppFC.total)})` : ""
            }${ppStyle}`,
        },
      ],
    },
    !filterMods || mods.includes(parseInt(pb.enabled_mods))
      ? ""
      : "С этими модами прохождений нет, но есть это:",
    bm.beatmap_id,
  ];
};

module.exports.getMapTopEmbed = async (id, type, count, args) => {
  var bm = undefined;
  if (type == "b") bm = await getBeatmap(id);
  else if (type == "s") bm = await getBeatmapS(id);
  if (bm.approved <= 0) throw "noLeaderboard";

  const parser = new oppai.parser();
  parser.feed(bm.osuFile);

  var mods = args.match(/(?:^|\s)\+(\S+)(?:$|\s)/) || [];
  let filterMods = mods.length !== 0;
  mods = osuMods.parseModToken(mods[1]);

  var top = await getMapTop(
    bm.beatmap_id,
    parser.map,
    filterMods ? mods : null,
    count
  );
  count = top.length;
  const commondMods = osuMods.getCommonMods(
    top.map((p) => parseInt(p.enabled_mods))
  );

  const stars = new oppai.diff().calc({ map: parser.map, mods: commondMods });

  const [thumb, title, desc] = await makeBeatmapInfo(bm, commondMods, stars);
  const ppStyle = bm.approved > 2 ? "~~" : "";

  let fields = [];
  if (count <= 5) {
    for (let p of top) {
      let acc = new oppai.std_accuracy({
        n300: parseInt(p.count300),
        n100: parseInt(p.count100),
        n50: parseInt(p.count50),
        nmiss: parseInt(p.countmiss),
      });

      fields.push({
        name:
          `${rankEmojis[p.rank]} ${_u.esc(p.username)} ` +
          `${
            parseInt(p.enabled_mods)
              ? `+${osuMods.toString(parseInt(p.enabled_mods))} `
              : ""
          }` +
          `(${timeago.format(p.date + " +0000", "en-US")})`,
        value:
          `${_u.t(p.score)} **· ${_u.r(100 * acc.value())}% ·** ` +
          `${parseInt(p.perfect) ? "FC" : p.maxcombo + "x"} **·** ` +
          `${p.count300} ⁄ ${p.count100} ⁄ ${p.count50} ⁄ ${p.countmiss} **·** ` +
          `${ppStyle}**${_u.r(parseFloat(p.pp))}pp**${ppStyle}`,
      });
    }
  } else {
    for (let i = 1; i <= count; ) {
      let start = i,
        end = i + 4;
      if (end == count - 1) end++;
      if (end > count) end = count;

      let val = "";
      for (let j = start - 1; j < end; j++) {
        let acc = new oppai.std_accuracy({
          n300: parseInt(top[j].count300),
          n100: parseInt(top[j].count100),
          n50: parseInt(top[j].count50),
          nmiss: parseInt(top[j].countmiss),
        });
        let rank = top[j].rank[0].replace("X", "SS");
        val +=
          `**#${j + 1}** ${_u.esc(top[j].username)}` +
          `${
            parseInt(top[j].enabled_mods)
              ? ` +${osuMods.toString(parseInt(top[j].enabled_mods))}`
              : ""
          } **·** ` +
          `${timeago.format(top[j].date + " +0000", "en-US")} **·** ` +
          `${_u.r(100 * acc.value())}% **${rank}**`;
        if (j < end - 1) val += "\n";
      }
      fields.push({ name: `#${start}–#${end}`, value: val });
      i = end + 1;
    }
  }

  return [
    {
      color: skyColor,
      thumbnail: { url: thumb },
      title: title,
      description: desc,
      fields: fields,
    },
    "",
    bm.beatmap_id,
  ];
};

async function bloodcatFind(artist, title, diff, mapper) {
  console.debug(artist, title, diff, mapper);

  const query = `${artist || ""} ${title} ${diff || ""}`.replace(
    /[^a-z0-9\s]/gi,
    " "
  );
  const queryExt = `${query} ${mapper || ""}`.replace(/[^a-z0-9\s]/gi, " ");
  let mapsets;
  try {
    mapsets = (
      await axios.get(`https://bloodcat.com/osu/?q=${queryExt}&m=0&mod=json`, {
        timeout: 3000,
        headers: { "user-agent": `iltrof's Discord bot (iltrofx@gmail.com)` },
      })
    ).data;
    if (mapsets.length === 0) {
      mapsets = (
        await axios.get(`https://bloodcat.com/osu/?q=${query}&m=0&mod=json`, {
          timeout: 3000,
          headers: { "user-agent": `iltrof's Discord bot (iltrofx@gmail.com)` },
        })
      ).data;
      if (mapsets.length === 0) throw "noBeatmap";
      else if (mapsets.length === 61) {
        let page = 2;
        let response = [];
        do {
          response = (
            await axios.get(
              `https://bloodcat.com/osu/?q=${query}&p=${page}&m=0&mod=json`,
              {
                timeout: 3000,
                headers: {
                  "user-agent": `iltrof's Discord bot (iltrofx@gmail.com)`,
                },
              }
            )
          ).data;
          mapsets = mapsets.concat(response);
          page++;
        } while (response.length === 61);
      }
    } else if (mapsets.length === 61) {
      let page = 2;
      let response = [];
      do {
        response = (
          await axios.get(
            `https://bloodcat.com/osu/?q=${queryExt}&p=${page}&m=0&mod=json`,
            {
              timeout: 3000,
              headers: {
                "user-agent": `iltrof's Discord bot (iltrofx@gmail.com)`,
              },
            }
          )
        ).data;
        mapsets = mapsets.concat(response);
        page++;
      } while (response.length === 61);
    }
  } catch (err) {
    if (err.code === "ECONNABORTED") throw "bloodcatTimeout";
    else throw err;
  }

  let maps = [];
  for (let s of mapsets) {
    for (let b of s.beatmaps) {
      if (b.mode === "0")
        maps.push([
          s.artist,
          s.title,
          b.name,
          s.creator,
          s.status,
          b.star,
          b.id,
        ]);
    }
  }

  console.debug(maps);
  let sims = [];
  const q = [artist, title, diff, mapper];
  console.debug(q);
  for (let m of maps) {
    sims.push([_u.queryMatch(m, q), m]);
  }

  sims = sims.sort((a, b) => (b[0] !== a[0] ? b[0] - a[0] : b[1][5] - a[1][5]));

  console.debug(sims);
  return sims[0][1][6];
}

async function osusearchFind(artist, title, diff, mapper) {
  try {
    var response = (
      await axios.get(
        `https://osusearch.com/query/?title=${title}&artist=${
          artist || ""
        }&mapper=${mapper}`,
        {
          timeout: 5000,
          headers: { "user-agent": `iltrof's Discord bot (iltrofx@gmail.com)` },
        }
      )
    ).data;

    if (response.result_count <= 0) throw "noBeatmap";

    var maps = [];
    for (let set of response.beatmaps) {
      let bms = (
        await axios.get(
          `https://osusearch.com/getset/?beatmapset_id=${set.beatmapset_id}`,
          {
            timeout: 5000,
            headers: {
              "user-agent": `iltrof's Discord bot (iltrofx@gmail.com)`,
            },
          }
        )
      ).data;

      for (let bm of bms) {
        if (bm.gamemode == 0)
          maps.push([
            bm.artist,
            bm.title,
            bm.difficulty_name,
            bm.mapper,
            bm.beatmap_status,
            bm.difficulty,
            bm.beatmap_id,
          ]);
      }
    }
  } catch (err) {
    if (err.code === "ECONNABORTED") throw "osusearchTimeout";
    else throw err;
  }

  console.debug(maps);

  let sims = [];
  const q = [artist, title, diff, mapper];
  for (let m of maps) {
    sims.push([_u.queryMatch(m, q), m]);
  }

  sims = sims.sort((a, b) => (b[0] !== a[0] ? b[0] - a[0] : b[1][5] - a[1][5]));

  console.debug(sims);
  return sims[0][1][6];
}

module.exports.find = async (artist, title, diff, mapper) => {
  if (artist === "") artist = undefined;
  if (title === "") title = undefined;
  if (diff === "") diff = undefined;
  if (mapper === "") mapper = undefined;
  if (!title) throw "noBeatmap";

  try {
    return await bloodcatFind(artist, title, diff, mapper);
  } catch (err) {
    if (!mapper) {
      if (err === "bloodcatTimeout") throw "searchTimeout";
      else throw err;
    }
  }

  try {
    return await osusearchFind(artist, title, diff, mapper);
  } catch (err) {
    if (err === "osusearchTimeout") throw "searchTimeout";
    else throw err;
  }
};

module.exports.getIDFromScreenshot = async (url) => {
  try {
    var image = await axios.get(url, {
      timeout: 7000,
      responseType: "arraybuffer",
    });
    if (!image.headers["content-type"].match(/image\/.*/i)) return undefined;
  } catch (err) {
    if (err.code === "ECONNABORTED") throw "timeout";
    else throw err;
  }

  var uid = crypto.randomBytes(10).toString("hex");

  image = sharp(image.data);
  let md = await image.metadata();
  await image
    .extract({
      left: 0,
      top: 0,
      width: md.width,
      height: Math.floor(md.height / 8),
    })
    .resize(md.width < 1248 ? 1248 : md.width)
    .toFile(`cache/screenshots/${uid}.png`);

  execSync(
    `"C:/Program Files (x86)/Tesseract-OCR/tesseract" -l aller cache/screenshots/${uid}.png cache/screenshots/${uid}`
  );
  let lines = fs
    .readFileSync(`cache/screenshots/${uid}.txt`)
    .toString()
    .split("\n");

  var artist, title, diff, mapper;
  var firstLineFound = false;
  for (let line of lines) {
    if (line.trim().length < 4) continue;
    if (line.match(/^beatmap by/i)) {
      mapper = line.slice(10).trim();
    } else if (
      !firstLineFound &&
      line.indexOf("-") !== -1 &&
      line.indexOf("[") !== -1
    ) {
      diff = line.slice(line.lastIndexOf("[") + 1).trim();
      if (diff.endsWith("]")) diff = diff.slice(0, diff.length - 1);

      let artistTitle = line.slice(0, line.indexOf("["));
      artist = artistTitle.slice(0, line.indexOf("-")).trim();
      title = artistTitle.slice(line.lastIndexOf("-") + 1).trim();
      firstLineFound = true;
    }
  }

  if (!artist || !title || !diff || !mapper) return undefined;
  return await module.exports.find(artist, title, diff, mapper);
};
