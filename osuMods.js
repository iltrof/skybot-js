const oppai = require('ojsama');
oppai.modbits.sd = 32;
oppai.modbits.pf = 16384;

const mods = {nm: 0, nf: 1<<0, ez: 1<<1, td: 1<<2, hd: 1<<3,
              hr: 1<<4, sd: 1<<5, dt: 1<<6, ht: 1<<8,
              nc: 1<<9, fl: 1<<10, so: 1<<12, pf: 1<<14};
const order = "ez hd hr ht dt nc fl nf so sd pf td".split(' ');

module.exports.fromString = (modsStr) => {
    var res = oppai.modbits.from_string(modsStr);
    if(res & mods.nc) res |= mods.dt;
    if(res & mods.pf) res |= mods.sd;
    return res;
};

module.exports.toString = (modsInt) => {
    if(modsInt == 0) return 'Nomod';

    var res = '';
    for(var m of order) {
        if(mods[m] & modsInt)
            res += m.toUpperCase();
    }

    if(res.indexOf('NC') >= 0) res = res.replace('DT', '');
    if(res.indexOf('PF') >= 0) res = res.replace('SD', '');

    return res;
};

function parseModSubtoken(token) {
    token = token.toLowerCase();
    token = token.replace(/\((.*)\)/g,'$1');
    token = token.replace(/(.*)\?/g,'/$1');
    token = token.replace(/(dt|nc)\*/g,'dt/nc');
    token = token.replace(/(sd|pf)\*/g,'sd/pf');
    token = token.replace(/nm/g, '');
    return [...new Set(token.split('/'))];
}

function decodeModsString(modsStr) {
    return modsStr.match(/\(.*\)|..\*?\/..\*?|..\*?\??|../g).map(parseModSubtoken);
}

function genMods(modsArr) {
    if(modsArr.length === 0)
        return [''];

    var result = [];
    for(var m of modsArr[0]) {
        for(var ms of genMods(modsArr.slice(1))) {
            result.push(m+ms);
            if(result.length > 8) throw 'tooManyMods';
        }
    }

    return result;
}

module.exports.parseModToken = (token) => {
    if(!token) return [0];
    return genMods(decodeModsString(token)).map(module.exports.fromString);
};

module.exports.getCommonMods = (modsArr) => {
    var res = -1;
    for(var m of modsArr) {
        res &= m;
    }
    return res;
};